extends Area2D
class_name Worm

const SEGMENT = preload("res://Segment/Segment.tscn")

@onready var timer = $Timer
@onready var skins = $Skins

var move_distance: float = 32.0 # px / timeout.
var direction: Vector2 = Vector2.ZERO
var segments: Array[Segment]
var prior_segment: Segment
var prior_pos: Vector2 = Vector2(-500, -500)
var segment_count: int = 0

func _ready():
	timer.timeout.connect(OnMoveAlong)
	RandomSkin()
	AddSegments(4)

func _process(delta):
	# var direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	
	if Input.is_action_pressed("move_left") and direction != Vector2.RIGHT:
		direction = Vector2.LEFT
	elif Input.is_action_pressed("move_right") and direction != Vector2.LEFT:
		direction = Vector2.RIGHT
	elif Input.is_action_pressed("move_up") and direction != Vector2.DOWN:
		direction = Vector2.UP
	elif Input.is_action_pressed("move_down") and direction != Vector2.UP:
		direction = Vector2.DOWN

func RandomSkin():
	var rando = randi_range(0, skins.get_child_count() - 1)
	print(rando)
	for i in skins.get_child_count():
		if i == rando:
			skins.get_child(i).show()
		else:
			skins.get_child(i).hide()

func OnMoveAlong():
	prior_pos = position
	position += direction * move_distance
	
	if segment_count < segments.size():
		segment_count += 1
	
	UpdateTail()

func AddSegments(count: int):

	for i in count:
		var segment = SEGMENT.instantiate()
		segments.append(segment)
		get_parent().call_deferred("add_child", segment)
		segment.global_position = segments[segments.size() - 1].global_position
	timer.wait_time -= 0.03
	if timer.wait_time < 0.1:
		timer.wait_time = 0.1

func UpdateTail():
	for segment in segments:
		var prior_p = segment.position
		segment.position = prior_pos
		prior_pos = prior_p
	#
	#for i in segments.size():
		#if prior_segment:
			#segments[i].position = prior_segment.position
		#else:
			#segments[i].position = position
		#
		#prior_segment = segments[i]
	#prior_segment = null
