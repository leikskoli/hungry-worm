extends Node
class_name Main

const WORM = preload("res://Worm/Wormulon.tscn")
const FOOD = preload("res://Food/Food.tscn")

@onready var menu = $Menu
@onready var worm_node: Node2D = $WormNode

var margin: float = 24
var worm: Worm

func _ready():
	menu.main = self
	ScoreBoard.hide()

func SpawnFood():
	var food = FOOD.instantiate()
	food.main = self
	food.position.x = randf_range(margin, get_viewport().size.x - margin)
	food.position.y = randf_range(margin, get_viewport().size.y - margin)
	worm_node.call_deferred("add_child", food)

func SpawnWorm():
	ScoreBoard.show()
	worm = WORM.instantiate()
	worm_node.add_child(worm)
	worm.position = get_viewport().size / 2
