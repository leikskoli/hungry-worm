extends Area2D
class_name Food

@onready var skins = $Skins
@onready var point_value_ui = $PointValueUI

var main: Main
var point_value: int = randi_range(1, 9)

func _ready():
	area_entered.connect(OnAreaEntered)
	point_value_ui.text = str(point_value)
	RandomSkin()

func OnAreaEntered(area):
	if area is Worm:
		ScoreBoard.UpdateScore(point_value)
		main.SpawnFood()
		main.worm.AddSegments(point_value)
		queue_free()

func RandomSkin():
	var rando = randi_range(0, skins.get_child_count() - 1)
	print(rando)
	for i in skins.get_child_count():
		if i == rando:
			skins.get_child(i).show()
		else:
			skins.get_child(i).hide()
