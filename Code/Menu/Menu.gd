extends Control
class_name Menu

@onready var play_button = $VBox/PlayButton
@onready var quit_button = $VBox/QuitButton

@export var main: Main


func _ready():
	play_button.pressed.connect(OnPlayPressed)
	quit_button.pressed.connect(OnQuitPressed)


func _process(delta):
	if Input.is_action_just_pressed("pause"):
		show()


func OnPlayPressed():
	hide()
	main.SpawnWorm()
	main.SpawnFood()
	ScoreBoard.show()


func OnQuitPressed():
	get_tree().quit()
