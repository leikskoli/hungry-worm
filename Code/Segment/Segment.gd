extends Area2D
class_name Segment

@onready var skins = $Skins

func _ready():
	RandomSkin()

func RandomSkin():
	var rando = randi_range(0, skins.get_child_count() - 1)
	print(rando)
	for i in skins.get_child_count():
		if i == rando:
			skins.get_child(i).show()
		else:
			skins.get_child(i).hide()
