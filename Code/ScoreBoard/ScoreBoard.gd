extends PanelContainer
#class_name ScoreBoard

@onready var score_ui: Label = $ScoreUI
var score: int = 0

func _ready():
	Reset()

func Reset():
	score = 0
	score_ui.text = str(score)

func UpdateScore(value: int):
	score += value
	score_ui.text = str(score)
